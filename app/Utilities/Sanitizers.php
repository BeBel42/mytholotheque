<?php

namespace App\Utilities;

// removes all uris from text, and put them in returned array
function remove_uris(string &$org): array
{
	// removed uris will be stored here
	$uris = [];

	// to find urls
	$regex = [
		'#\[\[([^\\[\\]]*?)\]\]#s',
		'#\[\[([^\\[\\]]*?)\]\[([^\\[\\]]*?)\]\]#s',
	];

	// remove all uris, and put them in array
	$callback  = function (array $matches) use (&$uris): string {
		// pushing uri
		array_push($uris, $matches[1]);

		// removing it from org string
		if (sizeof($matches) === 3) {
			// [[url][desc]]
			return '[[][' . $matches[2] . ']]';
		} else {
			// [[url]]
			return '[[]]';
		}
	};

	// encode all urls
	$org = preg_replace_callback($regex, $callback, $org);

	// return all removed uris
	return $uris;
}

// put uris back into their places + sanitize them
function put_uris_back(string &$org, array $uris): void
{
	// reversse array because we will pop() every element
	$uris = array_reverse($uris);

	// find empty urls
	$regex = [
		'#\[\[\]\]#s',
		'#\[\[\]\[([^\\[\\]]*?)\]\]#s',
	];

	// callback for each regex match
	$callback = function (array $matches) use (&$uris): string {
		// decoded uri
		$uri = filter_var(array_pop($uris), FILTER_SANITIZE_URL);

		// [[url][desc]]
		if (sizeof($matches) === 2) {
			return "<a data-org-generated href='"
				. $uri
				. "'>"
				. $matches[1]
				. '</a>';
		}
		// [[url]]
		else {
			return "<a data-org-generated href='"
				. $uri
				. "'>"
				. $uri
				. '</a>';
		}
	};

	// decode all urls
	$org = preg_replace_callback($regex, $callback, $org);
}

// reaplacing decorations wtih proper tag
function set_line_breaks(string &$org): void
{
	$find = '#\n#s';
	$replace = "<br data-org-generated>";
	$org = preg_replace($find, $replace, $org);
}

// reaplacing decorations wtih proper tag
function set_tags(string &$org): void
{
	// supported org decorations
	$decorations = [
		'/' => 'i', // has to be first! (because / is end of html tag)
		'\*' => 'b',
		'_' => 'u',
		'\+' => 'del',
	];

	// generate regex for every decoration
	$find = array_map(function (string $char): string {
		return '#' . $char . '(.*?)' . $char . '#s';
	}, array_keys($decorations));

	// generate html for every decoration
	$replace = array_map(function (string $tag): string {
		return "<{$tag} data-org-generated>" . '${1}' . "</{$tag}>";
	}, array_values($decorations));

	// find and replace decoration by html
	$org = preg_replace($find, $replace, $org);
}

// encoding escaped characters
function encode_escaped(string &$org): void
{
	// escapable character => html code
	$entities = [
		'\*' => 42,
		'_' => 137,
		'/' => 47,
		'\+' => 43,
		'\[' => 91,
		'\]' => 93,
	];

	// generate regex for every escapable character
	$find = array_map(function (string $char): string {
		return '#\\\\' . $char . '#s';
	}, array_keys($entities));

	// generate html code for every escapables character
	$replace = array_map(function (string $n): string {
		return "&#92;&#;" . $n . ';';
	}, array_values($entities));

	// find and replace escapable characters by html code
	$org = preg_replace($find, $replace, $org);
}

/*
  *bold* // <b>
  _underlined_ // <u>
  /italic/ // <i>
  +strikethrough+ // <del>
  [[LINK][DESCRIPTION]]
  [[LINK]]

   Characters can be escaped by a '\'
   All generated tags will have the data-org-generated tag attached to it
*/
function org_to_html(string $org): string
{
	// sanitizing
	$org = htmlspecialchars($org);

	// remove uris from org string
	$uris = remove_uris($org);

	// encoding escaped characters
	encode_escaped($org);

	// reaplacing decorations wtih proper tag
	set_tags($org);

	// replacing \n by <br>
	set_line_breaks($org);

	// put uris back to org string + sanitize them
	put_uris_back($org, $uris);

	return $org;
}
