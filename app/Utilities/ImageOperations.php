<?php

namespace App\Utilities;

/*
   Takes binary data representing image.
   Takes two integers > 0 to deine their new size.
   It will adapt in case one or both are null.
   Returns resized binary image.
 */
function resize_image(string $data, int | null $w, int | null $h): \Intervention\Image\Image
{
	// how many params are there?
	$present_count = ($w !== null) + ($h !== null);

	// creating img
	$img = \Image::make($data);

	// resizing img if needed
	if ($present_count !== 0) {
		$img->resize(
			$w,
			$h,
			function ($constraint) use ($present_count) {
				// cannot be bigger than original
				$constraint->upSize();
				// keep aspect ratio if only one param
				if ($present_count === 1) {
					$constraint->aspectRatio();
				}
			}
		);
	}

	// returning img with headers
	return $img;
}
