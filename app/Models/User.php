<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

require_once 'Services/Libravatar.php';

class User extends Authenticatable implements MustVerifyEmail
{
	use HasApiTokens, HasFactory, Notifiable;


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
		'role_id',
		'profile_picture'
	];

	// user if accoutn does not exist - or is deleted
	public static function nobody(): object
	{
		return new class {
			public string $name = '<utilisateur effacé>';
			public string $email = '<email inconnu>';
			public function avatar_url(int $size =  255): string {
				$sla = new \Services_Libravatar();
				$sla->setSize($size)->setDefault('identicon');
				$url = $sla->getUrl($this->email);
				return $url;
			}
		};
	}

	public function role(): belongsTo
	{
		return $this->belongsTo(Role::class);
	}

	public function owns(Model $model): bool
	{
		$owner = $model->user_id;
		return $owner === $this->id;
	}

	public function controls(Model $model): bool
	{
		$role = $this->role();
		if ($role == 'administator' or $role == 'moderator') {
			return true;
		}
		return $this->owns($model);
	}

	public function avatar_url(int $size = 255): string
	{
		// if pfp is present - return normal url
		if ($this->profile_picture != null) {
			return '/pfp/' . $this->id . '?s=' . $size;
		}

		// no pfp - search on libreavatar
		$sla = new \Services_Libravatar();
		$sla->setSize($size)->setDefault('identicon');
		$url = $sla->getUrl($this->email);
		return $url;
	}

	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array<int, string>
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array<string, string>
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'password' => 'hashed',
	];
}
