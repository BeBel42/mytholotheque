<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	use HasFactory;

	// args
	protected $fillable = ['parent_id', 'name'];

	// default values
	protected $attributes = [
		'name' => 'New Category',
	];
}
