<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Image;

class Paragraph extends Model
{
    use HasFactory;

    protected $fillable = ['god_id', 'index', 'title', 'body', 'image_id'];

	public function image() : HasOne
	{
		return $this->hasOne(Image::class);
	}
}
