<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    // args
    protected $fillable = ['name'];

	// no created_at and updated_at
	public $timestamps = false;
}
