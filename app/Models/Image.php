<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Image extends Model
{
	use HasFactory;
	use Searchable;

	public function toSearchableArray()
	{
		return [
			'id' => (int) $this->id,
			'alt' => $this->alt,
			'author' => $this->author,
		];
	}

	// args
	protected $fillable = ['alt', 'data', 'author'];
}
