<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mythology extends Model
{
    use HasFactory;

    // args
    protected $fillable = ['name'];
}
