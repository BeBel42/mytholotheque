<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Mythology;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class God extends Model
{
    use HasFactory;

    // args
    protected $fillable = ['name', 'category_id', 'public', 'mythology_id'];

    // default values
    protected $attributes = [
        'name' => 'Nouveau Dieu',
        'public' => false,
		'mythology_id' => 1,
    ];

	// return all paragraphs belonging to god
	public function paragraphs() : HasMany
	{
		return $this->hasMany(Paragraph::class);
	}

	// return mythology of god
	public function mythology() : BelongsTo
	{
		return $this->belongsTo(Mythology::class);
	}
}
