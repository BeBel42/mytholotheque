<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\God;
use App\Models\Mythology;
class CategoryController extends Controller
{
	// show category page to the public
	public function show(Category $category)
	{
		\Log::debug('Showing Category ' . $category->id);


		// get all parent categories
		$hierarchy = [];
		$iter = $category->id;
		while ($iter !== null) {
			$model = Category::find($iter);
			array_unshift($hierarchy, $model);
			$iter = $model->parent_id;
		}
		$hierarchy = array_map(fn ($x) => [
			'name' => $x->name,
			'link' => route('show.category', ['category' => $x->id])
		], $hierarchy);

		// get all children categories
		$children_categories = Category::where('parent_id', $category->id)
			->select('id', 'name')
			->orderBy('name')
			->get()
			->toArray();
		// get all children gods
		$children_gods = God::where('category_id', $category->id)
			->select('id', 'name', 'public', 'mythology_id')
			->orderBy('name')
			->get()
			->toArray();

		// mapping children to make them fit together
		$children_categories = array_map(
			fn (array $x): array => [
				"id" => $x['id'],
				'name' => $x["name"],
				'raw_name' => $x["name"],
				"type" => "category",
				"tags" => ['catégorie'],
			],
			$children_categories
		);
		$children_gods = array_map(
			fn (array $x): array => [
				"id" => $x['id'],
				'name' => $x["name"],
				'raw_name' => $x["name"],
				"type" => "god",
				"tags" => ['dieu', $x["public"] ? 'publique ' : 'privé', Mythology::find($x['mythology_id'])->name],
				"mythology_id" => $x['mythology_id'],
				"public" => $x["public"]
			],
			$children_gods
		);

		// merge children in one array
		$children = array_merge($children_categories, $children_gods);

		// return category view with variables
		return view('show.category', [
			'category' => $category,
			'hierarchy' => $hierarchy,
			'children' => $children,
		]);
	}
	// create one category
	public function post(Request $request)
	{
		$fields = $request->validate([
			'parent_id' => ['numeric', 'min:0', 'required'],
			'name' => ['max:100'],
		]);
		Log::debug('Creating Category with parent: ' . $fields['parent_id']);
		$new_cat = Category::create($fields);
		return response()->json($new_cat, 201);
	}

	// return all categories as JSON
	public function all()
	{
		Log::debug('Getting all Categories');
		$categories = Category::select('id', 'name', 'parent_id')->get();
		return response()->json($categories, 200);
	}

	// delete category from id
	public function delete(Category $category)
	{
		Log::debug('Deleting Category ' . $category->id);
		if ($category->id == 1) {
			return response('Cannot delete root category', 400);
		}
		$category->delete();
		return response('Category has been deleted', 200);
	}

	// patch one category
	public function patch(Category $category, Request $request)
	{
		Log::debug('Patching category ' . $category->id);
		if ($category->id == 1) {
			return response('Cannot patch root category', 400);
		}

		$fields = $request->validate([
			'name' => ['max:100', 'min:1'],
		]);

		$category->update($fields);
		return response()->noContent();
	}
}
