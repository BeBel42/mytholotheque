<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use App\Models\God;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;

use function App\Utilities\org_to_html;

class ThreadController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index(God $god)
	{
		$threads = $god
			->hasMany(Thread::class)
			->select('id', 'created_at', 'user_id', 'title')
			->get();
		foreach ($threads as $thread) {
			// find user by id
			$user = User::findOr(
				$thread['user_id'],
				// nobody class if none found
				fn () => User::nobody(),
			);
			// comment count
			$n_comments = $thread->hasMany(Comment::class)->get()->count();
			$thread['n_comments'] = $n_comments;
			$thread['user'] = $user;
			// title of thread
			$thread['html_title'] = org_to_html($thread['title']);
		}
		return view('show.threads', [
			'god' => $god,
			'threads' => $threads,
		]);
	}

	/**
	 * The resource
	 */
	public function show(Thread $thread)
	{
		$god = God::find($thread->god_id);
		$comments = $thread->hasMany(Comment::class)->get();
		foreach ($comments as $comment) {
			$user = User::findOr(
				$comment['user_id'],
				// nobody class if none found
				fn () => User::nobody(),
			);

			$comment['html_content'] = org_to_html($comment['content']);
			$comment['user'] = $user;
		}
		$thread['user_name'] = User::findOr(
			$thread['user_id'],
			fn () => '<deleted user>'
		)['name'];
		$thread['html_title'] = org_to_html($thread['title']);
		return view('show.thread', [
			'thread' => $thread,
			'god' => $god,
			'comments' => $comments,
		]);
	}

	// create one thread
	public function post(Request $request)
	{
		\Log::debug($request);
		$fields = $request->validate([
			'god_id' => ['required', 'numeric', 'min:1'],
			'title' => ['required', 'max:200'],
		]);
		$fields['user_id'] = auth()->id();
		\Log::debug('Creating Thread for god ' . $fields['god_id']);
		$thread = Thread::create($fields);
		return response()
			->redirectToRoute('show.thread', ['thread' => $thread->id]);
	}

	// delete thread from id
	public function delete(Thread $thread)
	{
		\Log::debug('Deleting Thread ' . $thread->id);
		$thread->delete();

		return response()
			->redirectToRoute('show.threads', ['god' => $thread->god_id])
			->with('status', 'Thread deleted successfully!');
	}

	public function patch(thread $thread, Request $request)
	{
		\Log::debug('Patching Thread ' . $thread->id);
		$fields = $request->validate([
			'title' => ['max:200', 'required'],
		]);
		$thread->update($fields);
		return redirect()
			->back()
			->with('status', 'Thread updated successfully!');
	}
}
