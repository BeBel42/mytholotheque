<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
	// create one comment
	public function post(Request $request)
	{
		\Log::debug($request);
		$fields = $request->validate([
			'thread_id' => ['required', 'numeric', 'min:1'],
			'content' => ['required', 'max:800'],
		]);
		$fields['user_id'] = auth()->id();
		\Log::debug('Creating comment for thread ' . $fields['thread_id']);
		Comment::create($fields);
		return redirect()->to(url()->previous() . '#bottom');	}

    // delete comment from id
    public function delete(Comment $comment)
    {
        \Log::debug('Deleting Comment ' . $comment->id);
		$comment->delete();
		return redirect()
			->back()
			->with('status', 'Comment deleted successfully!');
    }

	public function patch(Comment $comment, Request $request)
	{
		$fields = $request->validate([
			'content' => ['max:8000', 'required'],
		]);
		$comment->update($fields);
		return redirect()
			->back()
			->with('status', 'Comment updated successfully!');
	}
}
