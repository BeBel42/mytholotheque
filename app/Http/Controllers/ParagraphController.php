<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paragraph;

class ParagraphController extends Controller
{
    public function create(Request $request)
    {
        $fields = $request->validate([
            'god_id' => ['required', 'numeric', 'min:0'],
            'index' => ['required', 'numeric', 'min:0'],
            'title' => ['required', 'max:100'],
            'body' => ['required', 'max:5000'],
        ]);
        $new_par = Paragraph::create($fields);
        return response()->json($new_par, 201);
    }
}
