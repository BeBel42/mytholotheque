<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\God;
use App\Models\Paragraph;
use App\Models\Category;
use App\Models\Image;
use App\Models\Mythology;

use function App\Utilities\org_to_html;

class GodController extends Controller
{
	// create one god
	public function post(Request $request)
	{
		$fields = $request->validate([
			'category_id' => ['required', 'numeric', 'min:0'],
			'name' => ['max:100'],
		]);
		\Log::debug('Creating God from category ' . $fields['category_id']);
		$new_god = God::create($fields);
		return response()->json($new_god, 201);
	}

	// return all gods as json
	public function all()
	{
		\Log::debug('Getting all Gods');
		$gods = God::select('id', 'name', 'category_id')->get();
		return response()->json($gods, 200);
	}

	// delete god from id
	public function delete(God $god)
	{
		\Log::debug('Deleting God ' . $god->id);
		$god->delete();
		return response()->noContent();
	}

	// patch one god and its paragraphs
	public function patch(God $god, Request $request)
	{
		// validation
		\Log::debug('Patching god ' . $god->id);
		$fields = $request->validate([
			'god-name' => ['max:100', 'required'],
			'mythology_id' => ['numeric', 'required'],
			'public' => ['required', 'boolean'],
			'paragraphs' => ['array', 'required'],
			'paragraphs.*' => ['array', 'required'],
			'paragraphs.*.title' => ['nullable', 'max:100'],
			'paragraphs.*.body' => ['nullable', 'max:8000'],
			'paragraphs.*.image_id' => [
				'nullable', 'numeric', 'exists:App\Models\Image,id'
			],
		]);

		// mapping null text inputs to empty string
		$fields['paragraphs'] = array_map(function ($paragraph) {
			$p = $paragraph;
			$p['title'] = $p['title'] ?? '';
			$p['body'] = $p['body'] ?? '';
			return $p;
		}, $fields['paragraphs']);

		// chang emythology only if is valid
		$new_mythology_id = Mythology::find($fields['mythology_id'])
			? $fields['mythology_id']
			: $god->mythology_id;

		// updating god's properties
		$god->update([
			'name' => $fields['god-name'],
			'public' => $fields['public'],
			'mythology_id' => $new_mythology_id,
		]);

		// replacing old paragraphs by new ones
		$god->paragraphs()->delete();
		foreach ($fields['paragraphs'] as $i => $new_paragraph) {
			$new_paragraph['god_id'] = $god->id;
			$new_paragraph['index'] = $i;
			Paragraph::create($new_paragraph);
		}

		// return to view with success message
		return redirect()
			->back()
			->with('status', 'Dieu sauvegardé!');
	}

	// show god page to the public
	public function show(God $god)
	{
		\Log::debug('Showing God ' . $god->id);

		$is_allowed = (bool) auth()->check();
		if ($is_allowed) {
			$user_role = auth()->user()->role['name'];
			$is_allowed = ($user_role === 'administrator'
				|| $user_role === 'moderator');
		}

		// refuse to show god if is private and user is not admin
		if (!$god->public && !$is_allowed) {
			abort(403, 'God is private.');
		}

		foreach ($god->paragraphs as $paragraph) {
			// sanitizing and converting paragraphs to html
			$paragraph['html_body'] = org_to_html($paragraph['body']);
			$paragraph['html_title'] = org_to_html($paragraph['title']);

			// no image by default
			$paragraph['image'] = null;

			// if image, fill with data
			if ($paragraph['image_id'] !== null) {
				// getting all needed image fields
				$paragraph['image'] = Image::where('id', $paragraph['image_id'])
					->select('alt', 'author', 'id')
					->first();

				// converting org format to html
				$paragraph['image']['html_alt'] =
					org_to_html($paragraph['image']['alt']);
				$paragraph['image']['html_author'] =
					org_to_html($paragraph['image']['author']);
			}
		}

		// get all parent categories of god
		$categories = [];
		$iter = $god->category_id;
		while ($iter !== null) {
			$model = Category::find($iter);
			array_unshift($categories, $model);
			$iter = $model->parent_id;
		}

		// building parents array for head
		$hierarchy = array_map(
			fn ($x): array
			=> [
				'name' => $x->name,
				'link' => route('show.category', ['category' => $x->id])
			],
			$categories
		);
		// adding god at end of hierarchy array
		array_push($hierarchy, [
			'name' => $god->name,
			'link' => route('show.god', ['god' => $god->id])
		]);

		// return god view with variables
		return view('show.god', [
			'god' => $god,
			'hierarchy' => $hierarchy,
		]);
	}

	// return god editor for managing its content
	public function manage(God $god) {
		\Log::debug('Launching editor of god id ' . $god->id);

		// get all parent categories of god
		$categories = [];
		$iter = $god->category_id;
		while ($iter !== null) {
			$model = Category::find($iter);
			array_unshift($categories, $model);
			$iter = $model->parent_id;
		}

		// building parents array for head
		$hierarchy = array_map(
			fn ($x): array
			=> [
				'name' => $x->name,
				'link' => route('show.category', ['category' => $x->id])
			],
			$categories
		);
		// adding god at end of hierarchy array
		array_push($hierarchy, [
			'name' => $god->name,
			'link' => route('show.god', ['god' => $god->id])
		]);

		$mythologies = Mythology::all()->toArray();
		return view('manage.god', [
			'god' => $god,
			'mythologies' => $mythologies,
			'hierarchy' => $hierarchy,
		]);
	}
}
