<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

use function App\Utilities\org_to_html;
use function App\Utilities\resize_image;

class ImageController extends Controller
{
	 // Display a listing of the resource for quick editing
	public function manage(Request $request)
	{
		$fields = $request->validate([
			'search' => ['max:100'],
		]);

		// empty string means all
		$search = $fields['search'] ?? '';
		// 5 elems per page
		$images = Image::search($search)->query(
			// only selected needed fields
			fn($query) => $query->select('id', 'alt', 'author')
		)->orderBy('id')->paginate(5);

		// converting org formatting to html
		foreach ($images as $image) {
			$image['html_alt'] = org_to_html($image['alt']);
			$image['html_author'] = org_to_html($image['author']);
		}

		return view('manage.images', [
			'images' => $images,
		]);
	}

	// return json list of paginated images
	public function json_paginate(Request $request)
	{
		$fields = $request->validate([
			'search' => ['max:100'],
		]);

		// empty string means all
		$search = $fields['search'] ?? '';

		// 3 elems per page
		$images = Image::search($search)->query(
			// only selected needed fields
			fn($query) => $query->select('id', 'alt', 'author')
		)->orderBy('id')->paginate(3);

		// converting org formatting to html
		foreach ($images as $image) {
			$image['html_alt'] = org_to_html($image['alt']);
			$image['html_author'] = org_to_html($image['author']);
		}

		return $images;
	}

	 // Display the specified resource.
	public function show(Image $image, Request $request)
	{
		// custom validator
		$validator = Validator::make($request->all(), [
			'w' => ['numeric', 'min:1'],
			'h' => ['numeric', 'min:1'],
		]);

		// return json error if failure
		if ($validator->fails()) {
			throw new HttpResponseException(
				response()->json(
					$validator->errors(),
					422
				)
			);
		}

		// retrieve the validated input
		$fields = $validator->validated();

		// setting width and height variables
		$w = $fields['w'] ?? null;
		$h = $fields['h'] ?? null;

		// resize image according to params
		return resize_image($image->data, $w, $h)->response();
	}

	// Show the form for editing the specified resource.
	public function post(Request $request)
	{
		\Log::debug('Creating new image');
		\Log::debug($request);
		$fields = $request->validate([
			'image' => ['image', 'required'],
			'alt' => ['max:200', 'required'],
			'author' => ['max:200', 'required'],
		]);

		\Log::debug('calidated');
		$img_tmp = $request->file('image');
		$img_params = [
			'alt' => $fields['alt'],
			'data' => file_get_contents($img_tmp),
			'author' => $fields['author'],
		];
		Image::create($img_params);
		return redirect()
			->back()
			->with('status', 'Image envoyée avec succès !');
	}

	// deleting an image from id (no ajax)
	public function delete(Image $image)
	{
		\Log::debug('Deleting image ' . $image->id);
		$image->delete();
		return redirect()
			->back()
			->with('status', 'Image effacée avec succès !');
	}

	// patching an image from id (no ajax)
	public function patch(Image $image, Request $request)
	{
		\Log::debug('Patching image ' . $image->id);
		$fields = $request->validate([
			'image' => ['image'],
			'alt' => ['max:200'],
			'author' => ['max:200'],
		]);
		if (isset($fields['image'])) {
			$img_tmp = $request->file('image');
			$fields['data'] = file_get_contents($img_tmp);
		}
		$image->update($fields);
		return redirect()
			->back()
			->with('status', 'Image changée avec succès !');
	}
}
