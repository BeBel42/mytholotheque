<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

use function App\Utilities\resize_image;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

	/**
	 * Set new profile picture for user
	 */
	public function set_profile_picture(Request $request)
	{
		\Log::debug('Setting new profile picture');
		$fields = $request->validate([
			'image' => ['image', 'required'],
		]);
        // Get current user
		$img_tmp = $request->file('image');
		$data = file_get_contents($img_tmp);

		// make image smaler to store anything
		$data = resize_image($data, 255, 255)->encode('png');

		// $user->update(['profile_picture' => $data]);
		auth()->user()->profile_picture = $data;
		auth()->user()->save(); // lsp error - the function exists

		return redirect()
			->back()
			->with('status', 'Profile Picture uploaded successfully!');
	}

	/**
	 * Get binary of user's profile picture
	 */
	public function get_profile_picture(User $user, Request $request)
	{
		\Log::debug('Returning profile picture of user ' . $user->id);

		// custom validator
		$validator = Validator::make($request->all(), [
			's' => ['numeric', 'min:1'],
		]);

		// return json error if failure
		if ($validator->fails()) {
			throw new HttpResponseException(
				response()->json(
					$validator->errors(),
					422
				)
			);
		}

		// retrieve the validated input
		$fields = $validator->validated();

		// setting width and height variables
		$s = $fields['s'] ?? null;

		// resize image according to params
		return resize_image($user->profile_picture, $s, $s)->response();
	}

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
