<x-app-layout>
	<x-slot name="header">
		{{ __('Profile') }}
	</x-slot>

	<x-scripts.validate-size/>

	<div x-data='profileData' class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
			<div class="p-4 sm:p-8 shadow sm:rounded-lg">
				<div class="max-w-xl">

					<div class='flex items-end'>
						<form enctype='multipart/form-data' action='/pfp' method='POST' id='patch-form-pfp'>
							@method('PATCH')
							@csrf
							<img
								title='Change Profile Picture'
								@click='editPfp'
								class='cursor-pointer'
								height='100'
								src="{{  auth()->user()?->avatar_url(100) }}"
							/>
							<div id='edit-pfp-error' class='text-red-400'></div>
						</form>
					</div>

					@include('profile.partials.update-profile-information-form')
				</div>
			</div>

			<div class="p-4 sm:p-8 shadow sm:rounded-lg">
				<div class="max-w-xl">
					@include('profile.partials.update-password-form')
				</div>
			</div>

			<div class="p-4 sm:p-8 shadow sm:rounded-lg">
				<div class="max-w-xl">
					@include('profile.partials.delete-user-form')
				</div>
			</div>
		</div>
	</div>
</x-app-layout>

<script>
	window.profileData = {
		// change PFP
		editPfp() {
			// create hidden input
			const fileInput = document.createElement('input');
			fileInput.name = 'image';
			fileInput.classList.add('hidden');
			fileInput.type = 'file';

			// submit when file is selected - and small enough
			fileInput.addEventListener('change', function(e) {
				if (this.files
					&& this.files[0]
					&& validateSize(this, '#edit-pfp-error')) {
					form.submit();
				}
			});

			// get parent form
			const form = document.getElementById(`patch-form-pfp`);
			form.appendChild(fileInput);

			// open file select prompt
			fileInput.click();
		},
	}
</script>
