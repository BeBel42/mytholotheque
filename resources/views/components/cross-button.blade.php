<div class='cursor-pointer relative py-2'>
	<x-bladewind::button color='red' size='tiny' outline='true' border_width='0' can_submit='true'>
		<x-icons.x />
		<div style='position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);' class='transition hover:bg-red-400/20 rounded-full p-6'></div>
	</x-bladewind::button>
</div>
