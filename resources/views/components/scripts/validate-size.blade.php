<script>
"use strict";

/*
 * Pass this funciton on an elem's onchange
 * input -> usually this
 * errorElemQuery -> querySelector of elem that will contain error message
 *
 * returns false if not valid, true otherwise
 */
function validateSize(input, errorElemQuery) {
	const maxSize = 2; // MB

	const errorElem = document.querySelector(errorElemQuery);

	// check if element is found
	if (!errorElem) {
		console.error(`errorElemQuery is invalid: ${errorElemQuery} does not exist`);
		return false;
	}

	// for each file of input, see if it is small enough
	for (const file of input.files) {
		if (file.size > maxSize * 1000000) {
			errorElem.innerHTML = `Le fichier dépasse ${maxSize}MB`;
			input.value = '';
			return false;
		}
	}

	// did not return - all images are small enough
	errorElem.innerHTML = '';
	return true;
}
</script>
