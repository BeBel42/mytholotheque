<x-bladewind::modal
	name='confirm-modal-default'
	ok_button_action='onConfirmGlobal()'
    center_action_buttons="true"
	ok_button_label="Oui"
    cancel_button_label="Non"
>
	<h3 class='my-6 text-center text-2xl' id='modal-showmodal-content'>
		Voulez-vous vraiment effacer ce paragraphe ?
	</h3>
</x-bladewind::modal>

<script>
let onConfirmGlobal = () => {};

"use strict";

/*
 * Asks to comfirm for action
 * title is title of modal
 * onConfirm is callback if "Yes" is pressed
 * set shouldRemoveAfterAction to true if page does not reload on confirm
*/
function confirmModal(title, onConfirm, shouldRemoveAfterAction = false) {
	onConfirmGlobal = onConfirm;
	document.getElementById('modal-showmodal-content').innerHTML = title;
	return showModal('confirm-modal-default')
}
</script>
