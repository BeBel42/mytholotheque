@props(['hoverable' => false])

@php
$hover_class = '';
if ($hoverable) {
$hover_class = 'hover:border-yellow-400 transition';
}
@endphp

<div class='bg-gray-800 mb-10 mt-5 rounded-lg border border-red-900/0 {{$hover_class}}'>
	<div class='p-6'>
		{{ $slot }}
	</div>
	@if (isset($footer))
	<div class='text-sm border-gray-600 border-t text-gray-400 w-full px-6 py-2'>
		{{$footer}}
	</div>
	@endif
</div>
