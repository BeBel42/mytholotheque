@props(['user'])

<div class='flex items-center'>
	<div>
		<img class='rounded-full me-3' height='50' src="{{$user->avatar_url(50)}}" />
	</div>
	<div>
		<div>{{$user->name}}</div>
		<div class='border-t border-l border-yellow-300 rounded-tl w-full pb-1'></div>
	</div>
</div>
