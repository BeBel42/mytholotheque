<!-- Variable name containing id of parent category -->
@props(['parentId' => 'parentId'])

<div x-data='dropdownData'>
	<x-dropdown>

		<x-slot name="trigger">
			<button class="inline-flex
						   items-center
						   px-4
						   py-3
						   border
						   border-transparent
						   text-sm
						   leading-4
						   font-medium
						   rounded-md
						   text-gray-300
						   bg-gray-900
						   hover:text-yellow-300
						   focus:outline-none
						   transition
						   ease-in-out
						   duration-150">
				<div x-text='"Add for " + {{$parentId}}'></div>
				<div class="ml-1">
					<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
						<path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
					</svg>
				</div>
			</button>
		</x-slot>

		<x-slot name="content">
			<x-dropdown-div @click='createGod({{$parentId}})'>
				God
			</x-dropdown-div>
			<x-dropdown-div @click='createCategory({{$parentId}})'>
				Category
			</x-dropdown-div>
		</x-slot>

	</x-dropdown>
</div>

<script>
	window.dropdownData = {
		// 'God' section pressed
		createGod: async function(categoryId) {
			const postBody = {
				category_id: categoryId
			};
			if ((await axios.post('/god', postBody)).status === 201) {
				this.refreshGods();
			}
		},

		// 'Category' section pressed
		createCategory: async function(parentId) {
			const postBody = {
				parent_id: parentId
			};
			if ((await axios.post('/category', postBody)).status === 201) {
				this.refreshCategories();
			}
		},
	};
</script>
