@props(['hierarchy' => []])

<div class="flex justify-center shrink-0 items-center">
	<nav class='text-xs flex justify-center'>
		@foreach ($hierarchy as $index => $parent)
			<a href='{{$parent['link']}}' class='transition mx-1 hover:underline hover:text-yellow-300'>
				{{$parent['name']}}
			</a>
			@if ($index + 1 < sizeof($hierarchy))
			<span class='text-yellow-200 font-semibold'>&gt;</span>
			@endif
		@endforeach
	</nav>
</div>
