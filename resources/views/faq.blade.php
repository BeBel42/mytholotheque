<?php

use function App\Utilities\org_to_html;

$examples = [
	'/italique/',
	'*Gras*',
	'_Souligné_',
	'+Barré+',
	'[[https://httpdragons.com]]',
	'[[https://httpdragons.com][lien]]',
	'_Un mélange de *tout*_
	[[https://httpdragons.com][+lien barré+]]
	*Sur /plusieurs/
	lignes!*'
]
?>

<x-app-layout>
	<x-slot name="header">
		{{ __('FAQ') }}
	</x-slot>

	<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 py-6">
		<h2 class='text-xl font-bold'>Formattage</h2>
		<p>
			Il est possible de formatter votre texte en suivant la
			<a class='link' href='https://orgmode.org/'>syntaxe org</a>.<br>
			Les fonctionnalités supportées sont montreées dans
			les examples suivants :
		</p>
		<div class='border border-gray-400 w-fit rounded-lg'>
			<table class='table-fixed m-1 bg-gray-700'>
				<thead>
					<tr>
						<th class='p-3'>Texte</th>
						<th class='p-3'>Rendu</th>
					</tr>
				</thead>
				<tbody>
					@foreach($examples as $example)
					<tr class='odd:bg-gray-800'>
						<td class='p-3'><?= preg_replace('#\n#s', '<br>', $example) ?></td>
						<td class='p-3'><?= org_to_html($example) ?></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

	</div>

</x-app-layout>
