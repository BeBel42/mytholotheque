<!-- this view neds poroper html and body tags -->
<x-app-layout>

	<x-slot name='header'>

		@if (session('status'))
		<div class="mb-3 bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
			{{ session('status') }}
		</div>
		@endif

		<div>
			Comments of <a href='{{ route('show.god', ['god' => $god->id]) }}'>{{ $god->name }}</a>
		</div>
	</x-slot>

	<div class='my-8'>
		<h2>Create new Thread</h2>
		<form method='POST action=' /thread'>
			@csrf
			<input type='hidden' name='god_id' value='{{ $god->id }}' />
			<div class='my-4'>
				<input type='text' class='rounded w-1/2 bg-gray-800' name='title' />
			</div>
			<button class='btn-primary' type='submit'>Submit</button>
		</form>
	</div>

	<div>
		<h2>Peek At A Thread</h2>
		@foreach ($threads as $index => $thread)
		<a href='{{ route('show.thread', ['thread' => $thread->id]) }}'>
			<x-chat.card hoverable='true'>
				<div class='flex items-center justify-between'>
					<x-chat.user-pfp-name :user='$thread->user' />
					<span> {{ $thread->created_at }} </span>
				</div>
				<div class='my-4 text-xl'>
					<?= $thread->html_title ?>
				</div>
				<div class='w-fit flex p-2 mt-4 rounded-lg bg-gray-700'>
					<x-icons.chat-alt class='me-1' />
					{{ $thread->n_comments }}
				</div>
			</x-chat.card>
		</a>
		@endforeach
	</div>

</x-app-layout>
