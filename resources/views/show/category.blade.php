<?php
$is_mod = in_array(auth()->user()?->role['name'], ['administrator', 'moderator']);

// to convert tag to color according to string content
function string_to_color(string $word): string
{
	static $assigned_tags = [];
	static $color_index = 0;

	$tag_colors = [
		'violet',
		'royalblue',
		'yellowgreen',
		'red',
		'cyan',
		'aqua',
		'hotpink',
		'springgreen',
		'blue',
		'slateblue',
		'fuchsia',
		'orange',
		'darkorchid',
		'gold',
		'indigo',
	];

	if (isset($assigned_tags[$word])) {
		return $assigned_tags[$word];
	}

	$tag_color = $tag_colors[$color_index];
	$color_index = ($color_index + 1) % sizeof($tag_colors);
	$assigned_tags[$word] = $tag_color;

	return $tag_color;
}
?>

<x-app-layout>

	<x-scripts.confirm-modal />

	<x-slot name='header'>

		<div class='flex gap-4'>
			<div id='parent-category-name'><?= $category->name ?></div>
			@if ($is_mod and $category->id != 1)
			<div id='start-edit-button' onclick='toggleEditParent(true)' role='button' class='hover:text-primary-400 transition'>
				<x-icons.pencil size='4' />
			</div>
			<input onkeydown="if (event.key === 'Enter') applyEditParentName()" name='parent-category-input' class="hidden bg-gray-800" />
			<div class='flex flex-col justify-between'>
				<div id='apply-edit-parent' title='Valider nom' onclick='applyEditParentName()' role='button' class='hover:text-success-500 text-success-400 transition hidden'>
					<x-icons.check size='5' />
				</div>
				<div id='stop-edit-button' title='Annuler' onclick='toggleEditParent(false)' role='button' class='hover:text-danger-500 text-danger-400 transition hidden'>
					<x-icons.x size='5' />
				</div>
			</div>
			@endif
		</div>
	</x-slot>

	<x-navigation.hierarchy :hierarchy="$hierarchy" />


	<div x-data="showCategoryData">
		<div class='flex flex-col gap-5 h-screen'>

			@if ($is_mod)
			<div class='my-5'>
				<h2 class='text-xl'>
					Ajouter un élément
				</h2>


				<div class='mt-5'>
					<div class='flex gap-10 w-100'>
						<div>
							<x-bladewind::input name='name' class='bg-gray-800 h-12 rounded' />
						</div>
						<div class='w-full'>
							<x-bladewind::select required name="type" selected_value="Dieu" :data="[['label' => 'Dieu', 'value' => 'Dieu'], ['label' => 'Catégorie', 'value' => 'Catégorie']]" />
						</div>
					</div>
				</div>
				<button class='btn-primary' @click='createNew()'>
					Ajouter
				</button>
			</div>
			@endif

			<?php foreach ($children as $child) : ?>

				<?php
				# Skipping private gods for regular users
				if (!$is_mod and $child['type'] === 'god' and !$child['public'])
					continue;
				?>

				<?php
				$href = route('show.' . $child['type'], [$child['type'] => $child['id']]);
				?>

				<div :role="editingId === null ? 'button' : ''" @click="if(editingId === null) {
							window.location.href = '{{$href}}'
						 }
						 ">

					<div class='bg-gray-800 p-5 border border-yellow-400/25 hover:border-yellow-400 transition rounded'>
						<div class='flex justify-between h-full'>
							<div class='flex h-full flex-col justify-between'>


								<template x-if="!('{{$child['type']}}' === 'category' && editingId === {{ $child['id']}})">
									<div class='mb-4 text-lg'>
										<?= $child["name"] ?>
									</div>
								</template>
								<template x-if="('{{$child['type']}}' === 'category' && editingId === {{ $child['id']}})">
									<div class='flex gap-4 justify-center items-center'>
										<input id="category-{{$child['id']}}-name" @keydown="if (event.key === 'Enter') {
											   validateEdit({{$child['id']}})
											}
											" @click="event.stopPropagation();" class='bg-gray-800 h-10 mb-1' type='text' autofocus />

										<script>
											// autofocus the above input
											window.category_edit_input = document.getElementById("category-{{$child['id']}}-name");
											window.category_edit_input.focus();
											window.category_edit_input.value = '';
											window.category_edit_input.value = '<?= str_replace("'", "\'", $child["name"]) ?>';
										</script>


										<div class='flex flex-col justify-between'>
											<div title='Valider nom' @click="event.stopPropagation(); validateEdit({{$child['id']}});" role='button' class='hover:text-success-500 text-success-400 transition'>
												<x-icons.check size='5' />
											</div>
											<div title='Annuler' @click="event.stopPropagation(); editingId = null" role='button' class='hover:text-danger-500 text-danger-400 transition'>
												<x-icons.x size='5' />
											</div>
										</div>

									</div>
								</template>


								<div class='flex gap-5'>
									<?php foreach ($child['tags'] as $tag) : ?>
										<?php $tag_style = "style='background-color: color-mix(in srgb, " . string_to_color($tag) . " 50%, transparent);'"; ?>
										<div class='py-2 px-3 rounded text-xs' <?= $tag_style ?>>
											{{ $tag }}
										</div>
									<?php endforeach; ?>

								</div>
							</div>
							<div onclick='event.stopPropagation()'>


								@if ($is_mod)
								<div class="text-center pb-5 ps-5">
									<x-bladewind::dropmenu trigger_css='text-white' trigger="ellipsis-vertical-icon">
										<?php if ($child['type'] === 'category') : ?>
											<x-bladewind::dropmenu-item icon='pencil' @click="editingId = {{$child['id']}}; event.stopPropagation();">
												Éditer
											</x-bladewind::dropmenu-item>
										<?php else : ?>
											<x-bladewind::dropmenu-item icon='pencil' onclick="window.location.href = `{{ route('manage.god', ['god' => $child['id']]) }}`">
												Éditer
											</x-bladewind::dropmenu-item>
										<?php endif; ?>
										<?php
										$delete_link = '/' . $child['type'] . '/' . $child['id'];
										$prompt = $child['type'] === 'category' ? 'Voulez-vous vraiment effacer cette catégorie ?' : 'Voulez-vous vraiment effacer ce dieu ?';
										?>
										<x-bladewind::dropmenu-item icon='x-mark' onclick="confirmModal('{{ $prompt }}', async () => axios.delete('{{ $delete_link }}').then(() => location.reload()))">
											Effacer
										</x-bladewind::dropmenu-item>
									</x-bladewind::dropmenu>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

		</div>
	</div>

	<script>
		window.showCategoryData = {
			editingId: null,
			onSelect: (id) => {
				console.log(id);
			},
			validateEdit: async (id) => {
				const nameQS = `#category-${id}-name`;
				const name = document.querySelector(nameQS).value;

				await axios.patch(`/category/${id}`, {
					name,
				});

				location.reload();
			},
			createNew: async () => {
				const name = document.querySelector('input[name="name"]').value;
				const type = document.querySelector('input[name="type"]').value;
				const matchType = {
					'Dieu': 'god',
					'Catégorie': 'category'
				} [type];
				if (!matchType || !name) return;
				const parentId = <?= $category->id ?>;
				const parentKey = matchType === 'category' ? 'parent_id' : 'category_id';
				const postBody = {
					name: name,
				};
				postBody[parentKey] = parentId;
				const action = matchType === 'god' ? 'manage' : 'show';
				const res = await axios.post(`/${matchType}`, postBody);
				if (res.status) {
					const id = res.data.id;
					const url = `/${action}/${matchType}/${id}`;
					location.href = url;
				}
			}
		}

		/* non-alpine functions */

		// start editing parent name
		function toggleEditParent(shouldEdit) {
			const pcName = document.getElementById('parent-category-name');
			const pcInput = document.querySelector('input[name="parent-category-input"]');
			const startEditButton = document.getElementById('start-edit-button');
			const stopEditButton = document.getElementById('stop-edit-button');
			const applyEditParent = document.getElementById('apply-edit-parent');

			if (shouldEdit) {
				pcName.classList.add('hidden');
				pcInput.classList.remove('hidden')
				startEditButton.classList.add('hidden');
				stopEditButton.classList.remove('hidden');
				applyEditParent.classList.remove('hidden');
				pcInput.focus();
				pcInput.value = '';
				pcInput.value = '<?= str_replace("'", "\'", $category->name) ?>';
			} else {
				pcName.classList.remove('hidden');
				pcInput.classList.add('hidden')
				startEditButton.classList.remove('hidden');
				stopEditButton.classList.add('hidden');
				applyEditParent.classList.add('hidden');
			}
		}

		// apply new parent name
		async function applyEditParentName() {
			const pcInput = document.querySelector('input[name="parent-category-input"]');
			const name = pcInput.value;
			await axios.patch(`/category/{{ $category->id }}`, {
				name,
			});
			location.reload();
		}
	</script>


</x-app-layout>
