<!-- this view neds poroper html and body tags -->
<x-app-layout>

	<x-slot name='header'>

		@if (session('status'))
		<div class="mb-3 bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
			{{ session('status') }}
		</div>
		@endif

		<div>
			Comments of
			<a href='{{ route('show.god', ['god' => $god->id]) }}'>
				<u>
					{{ $god->name }}
				</u>
			</a>
		</div>
	</x-slot>

	<div x-data='commentsData'>

		<div class='my-3'>
			<a href='{{ route('show.threads', ['god' => $god->id]) }}'>
				<u>
					All threads
				</u>
			</a>
		</div>

		<div class='my-3'>
			<div>By {{ $thread->user_name }}</div>
			<div class='flex'>

				<template x-if="editing !== 'title'">
					<div><?= $thread->html_title ?></div>
				</template>
				<template x-if="editing === 'title'">
					<div>
						<form id='title-form' action='/thread/{{$thread->id}}' method='POST'>
							@method('PATCH')
							@csrf
							<input @keydown.enter='document.getElementById("title-form").submit()' class='bg-gray-800' value="{{$thread->title}}" name='title' />
						</form>
					</div>
				</template>

				@if (auth()->user()?->controls($thread))

				<button @click='if (editing === "title") editing = -1; else editing = "title"' type='button' class='btn-secondary'>
					<x-icons.pencil />
				</button>
				<form class='ms-1' method='POST' action='/thread/{{$thread->id}}'>
					@method('DELETE')
					@csrf
					<button type='submit' class='btn-danger p-1'>
						<x-icons.x />
					</button>
				</form>
				@endif
			</div>
		</div>
		@foreach ($comments as $comment)
		<x-chat.card>
			<x-chat.user-pfp-name :user="$comment->user" />
			<div>
				<template x-if="editing !== {{$comment->id}}">
					<div>
						<?= $comment->html_content ?>
					</div>
				</template>
				<template x-if="editing === {{$comment->id}}">
					<div>
						<form id='comment-form-{{$comment->id}}' action='/comment/{{$comment->id}}' method='POST'>
							@method('PATCH')
							@csrf
							<input @keydown.enter='document.getElementById("comment-form-{{$comment->id}}").submit()' class='bg-gray-800' value='{{$comment->content}}' name='content' />
						</form>
					</div>
				</template>
			</div>

			<x-slot name='footer'>
				<div class='flex justify-between items-center'>
					<div>{{ $comment->created_at  }}</div>
					<div class='flex'>
						@if (auth()->user()?->controls($comment))
						<button @click='if (editing === {{$comment->id}}) editing = -1; else editing = {{$comment->id}}' type='button' class='btn-secondary p-1'>
							<x-icons.pencil size='4' />
						</button>
						<form class='ms-1' method='POST' action='/comment/{{$comment->id}}'>
							@method('DELETE')
							@csrf
							<button type='submit' class='btn-danger p-1 ms-2'>
								<x-icons.x size='4' />
							</button>
						</form>
						@endif
					</div>
				</div>
			</x-slot>
		</x-chat.card>
		@endforeach

		<form method='POST' action='/comment'>
			@csrf
			<input type='hidden' name='thread_id' value='{{ $thread->id }}' />
			<textarea class='bg-gray-800' name='content'></textarea>
			<div>
				<button class='btn-primary' type='submit'>Submit Comment</button>
			</div>
		</form>

	</div>


</x-app-layout>

<script>
	window.commentsData = {
		editing: -1,
	}
</script>

<script type="text/javascript">
window.addEventListener('load', function () {
	window.scrollTo({
		left: 0,
		top: document.body.scrollHeight,
		behavior: 'instant'
	});
})
</script>
