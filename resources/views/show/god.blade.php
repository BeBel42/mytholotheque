<!-- this view neds poroper html and body tags -->
<x-app-layout>

	<x-slot name='header'>
		{{ $god->name }}
	</x-slot>

	<x-navigation.hierarchy :hierarchy="$hierarchy" />

	@foreach ($god->paragraphs as $index => $paragraph)
	@if ($index !== 0)
	<div class='mt-6'>
		@else
		<div>
			@endif
			<!-- Not sanitizing fields because passed to org_to_html() -->
			<h2 class='text-xl mb-3'><?= $paragraph['html_title'] ?></h2>

			@if ($paragraph['image'] !== null)
			<div class='bg-gray-800 p-5 rounded float-left me-3'>
				<img class='mb-3 float-left w-72' alt="<?= $paragraph['image']['alt'] ?>" src='{{ route('show.img', ['image' => $paragraph['image']['id'], 'w' => 288]) }}' />
				<div class='w-72 text-sm'>
					<div>
						<?= $paragraph['image']['html_alt'] ?>
					</div>
					<div>
						By <?= $paragraph['image']['html_author'] ?>
					</div>
				</div>
			</div>
			@endif

			<p><?= $paragraph['html_body'] ?></p>

		</div>

		<!-- To make next div under this div (needed cause I used float) -->
		<br clear='all'>
		@endforeach

		<a href='{{ route('show.threads', ['god' => $god->id]) }}' class='btn-secondary'>Comments</a>

	</div>

</x-app-layout>
