<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	@vite('resources/css/app.css')

	<title>Laravel</title>

	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
</head>

<body style='background-image: url("/images/background.jpg")' class='text-white'>

	<div class='h-screen bg-gradient-to-tl from-black/90 from-50%'>
		<div class='bg-gradient-to-b from-black/90 flex items-center p-3 pb-9 from-50%'>
			<a href='/' class='text-left text-xl flex-auto m-3 text-yellow-400'>Mytholotheque</a>
			@if (Route::has('login'))
			<div class='text-right flex-auto text-lg'>
				@auth
				<a class='m-3' href="{{ url('/dashboard') }}">
					{{__('Dashboard')}}
				</a>
				@else
				<a class='m-3' href="{{ route('login') }}">
					{{__('Log in')}}</a>
				@if (Route::has('register'))
				<a class='m-3 p-2 border rounded-lg' href="{{ route('register') }}">
					{{__('Register')}}
				</a>
				@endif
				@endauth
			</div>
			@endif
		</div>

		<div class='flex justify-center mt-9'>
			<div class='w-10/12 flex justify-between'>
				<div class='flex-auto'>
					<div class='text-7xl tracking-tighter'>La Bibliothèque d'Hadès</div>
					<div>Welcome to Mytholotheque!</div>
				</div>
				<img class='w-2/4' alt='Cerberus mascot guarding books' src='/images/cerberus.png' />
			</div>
		</div>
	</div>
</body>

</html>
