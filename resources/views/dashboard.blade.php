<x-app-layout>
	<x-slot name="header">
		{{ __('Dashboard') }}
	</x-slot>

	<div class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			@if (auth()->user()?->role['name'] == 'administrator')
			<div class="mb-6 p-6 bg-gray-800 text-gray-100 overflow-hidden shadow-sm sm:rounded-lg">
				<h2 class="my-6 text-xl">
					Admin Section
				</h2>

				<!-- Gestionnaire -->
				<div class="my-6 text-gray-900">
					<div class='mb-7'>
						<a class='btn-primary' href="{{ route('manage.images') }}">
							Gérer Images
						</a>
					</div>
					<div>
						<a class='btn-primary' href="{{ route('show.category', 1) }}">
							Gérer Categories
						</a>
					</div>
				</div>

			</div>
			@endif
			<div class="p-6 bg-gray-800 text-gray-100 overflow-hidden shadow-sm sm:rounded-lg">
				<h2 class="my-6 text-xl flex items-center justify-between">
					<div> {{ auth()->user()->name }} </div>
					<div class='text-sm'>Joined the {{ explode(' ', auth()->user()->created_at)[0] }}</div>
				</h2>
				<div>
					<div>{{ auth()->user()->role->name }}</div>
					<div>
						<a class='link' href='mailto:{{ auth()->user()->email }}'> {{ auth()->user()->email }}</a>
					</div>
					<div class='mt-6 flex justify-between'>

						<a class='btn-primary' href="{{ route('profile.edit') }}">
							Paramètres
						</a>
						<form method="POST" action="{{ route('logout') }}">
							@csrf
							<button type="submit" class="btn-danger">
								Déconnexion
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</x-app-layout>
