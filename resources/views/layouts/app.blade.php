<x-master-layout>
	<div class="min-h-screen pb-10">
		@include('layouts.navigation')
		<div>
			<!-- Page Heading -->
			@if (isset($header))
			<header>
				<div class="flex justify-center max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 my-6">
					<h2 class="font-semibold text-xl text-yellow-200 leading-tight">
						{{ $header }}
					</h2>
				</div>
			</header>
			@endif

			<!-- God category hierearchy -->
			@if (isset($parents))
			@endif
		</div>

		<!-- Page Content -->
		<main class='max-w-7xl mx-auto h-max'>
			{{ $slot }}
		</main>
	</div>
</x-master-layout>
