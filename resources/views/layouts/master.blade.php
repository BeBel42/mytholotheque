<!DOCTYPE html>

<!-- make dark mode enabled by default -->
<html class='dark' lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- bladewind ui library -->
	<link href="{{ asset('vendor/bladewind/css/animate.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('vendor/bladewind/css/bladewind-ui.min.css') }}" rel="stylesheet" />
	<script src="{{ asset('vendor/bladewind/js/helpers.js') }}"></script>

	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

	<!-- theme settings -->
	<x-head.disable-dark-reader />

	<!-- Scripts -->
	@vite([
	'resources/css/app.css',
	'resources/js/app.js',
	])

</head>

<body class="font-sans antialiased bg-gray-900 text-gray-100">
	{{ $slot }}
</body>

</html>
