<x-app-layout>
	<x-slot name="header">
		Gestionnaire d'Images
	</x-slot>

	<x-scripts.confirm-modal />
	<x-scripts.validate-size />

	<div x-data='imagesData' class='m-3 text-gray-200'>
		@if (session('status'))
		<div class="mb-3 bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
			{{ session('status') }}
		</div>
		@endif

		<div id='edit-img-error' class='text-red-400'></div>

		<div class='bg-gray-800 p-7 rounded my-8'>

			<form class='flex mb-7 justify-start items-center' method='GET'>
				@csrf

				<div class='flex items-center bg-gray-700 p-1'>
					<input value='{{app("request")->input('search')}}' class='focus:ring-0 border-none h-8 bg-gray-700 text-gray-100' name='search' type="text" placeholder="Recheche..." />
					<button type='submit' class='text-primary-400 hover:text-primary-300 active:text-primary-600 p-2 m-1 transition'>
						<x-icons.search size='6' />
					</button>
				</div>
			</form>

			@foreach ($images as $image)
			<div class='flex items-center mb-1 mt-1'>
				<form title='Effacer Image' class='me-2 inline' method='POST' @submit.prevent="confirmModal('Voulez-vous vraiment effacer cette image ?', () => $event.target.submit())" id='delete-{{ $image->id }}' action='/img/{{ $image->id }}'>
					@method('DELETE')
					@csrf
					<div>
						<x-cross-button />
					</div>
				</form>
				<div class='m-1 me-4 flex items-center'>
					{{ $image->id }}
				</div>

				<!-- Editable elements -->
				<form enctype='multipart/form-data' class='flex items-center' id='patch-form-{{ $image->id }}' method='POST' action='/img/{{ $image->id }}'>
					@method('PATCH')
					@csrf
					<div class='m-1 p-3 flex-none cursor-pointer transition transition rounded-lg hover:bg-yellow-700/40'>
						<img title='Changer Image' @click='editImg({{ $image->id }})' class='h-12' src='{{ route('show.img', ['image' => $image->id, 'h' => 48]) }}' />
					</div>
					<template x-if='editing.what === "alt" && editing.id === {{ $image->id }}'>
						<input @keydown.enter='document.getElementById("patch-form-{{ $image->id }}").submit();' name='alt' type='text' class='bg-gray-800 m-1 py-1 px-2 rounded' value="{{ $image->alt }}" />
					</template>
					<template x-if='!(editing.what === "alt" && editing.id === {{ $image->id }})'>
						<div @click='editing.what = "alt"; editing.id = {{ $image->id }}' class='m-1 truncate cursor-pointer py-3 px-6 transition rounded-lg hover:text-yellow-300 hover:bg-yellow-700/40' title='Changer Alt'>
							<?= $image->html_alt ?>
						</div>
					</template>
					<template x-if='editing.what === "author" && editing.id === {{ $image->id }}'>
						<input @keydown.enter='document.getElementById("patch-form-{{ $image->id }}").submit();' name='author' type='text' class='bg-gray-800 m-1 py-1 px-2 rounded' value="{{ $image->author }}" />
					</template>
					<template x-if='!(editing.what === "author" && editing.id === {{ $image->id }})'>
						<div @click='editing.what = "author"; editing.id = {{ $image->id }}' class='m-1 truncate cursor-pointer py-3 px-6 transition rounded-lg hover:text-yellow-300 hover:bg-yellow-700/40' title='Changer Auteur'>
							<?= $image->html_author ?>
						</div>
					</template>

				</form>
			</div>
			@endforeach
			<div class='mt-7'>
				{{ $images->links() }}
			</div>
		</div>

		<div class='mt-10'>
			<div class='text-xl'>Ajouter une image</div>
			<form method='POST' class='mt-2' action='/img' enctype="multipart/form-data">
				@csrf
				<div class='my-3 mb-0 flex flex-col w-3/12'>
					<label class='my-1' for='author-input'>Auteur</label>
					<input id='author-input' name='author' type='text' required class='bg-gray-800' />
				</div>
				<div class='my-3 flex flex-col w-3/12'>
					<label class='my-1' for='alt-input'>Description</label>
					<input id='alt-input' name='alt' class='bg-gray-800' type='text' required />
				</div>
				<div class='my-7 flex flex-col w-3/12'>
					<input id='file-input-id' name='image' @change="onFileUpload" type='file' required accept="image/*" hidden />
					<button title="Choisir une image" @click='document.getElementById("file-input-id").click()' type='button' class='btn-secondary d-flex justify-center'>Choisir image</button>
					<div id='file-name-input'></div>
					<div class='text-red-400' id='upload-error'>
					</div>
				</div>

				<button type='submit' class='btn-primary my-6 w-3/12 d-flex justify-center'>
					Envoyer
				</button>
			</form>
		</div>

	</div>
</x-app-layout>

<script>
	window.imagesData = {
		editing: {
			what: '', // "alt" or "author"
			id: null, // int
		},

		// submit other picture for img
		editImg(id) {
			// nothing is selected - only image is uploaded
			this.editing.what = "";
			this.editing.id = null;

			// create hidden input
			const fileInput = document.createElement('input');
			fileInput.name = 'image';
			fileInput.classList.add('hidden');
			fileInput.type = 'file';
			fileInput.accept = "image/*";

			// submit when file is selected
			fileInput.addEventListener('change', function(e) {
				if (this.files &&
					this.files[0] &&
					validateSize(this, '#edit-img-error')) {
					form.submit();
				}
			});

			// get parent form
			const form = document.getElementById(`patch-form-${id}`);
			form.appendChild(fileInput);

			// open file select prompt
			fileInput.click();
		},

		onFileUpload(event) {
			const input = event.target;
			validateSize(input, '#upload-error');
			document.getElementById('file-name-input').innerHTML = input.files?.[0]?.name ?? '';
		},
	}
</script>
