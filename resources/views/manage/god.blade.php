@php
$json_paragraphs = '[emptyParagraph("paragraph-0")]';

$json_paragraphs = [['title' => '', 'imageId' => null, 'body' => '', 'id' => 'paragraph-0',]];
foreach ($god->paragraphs as $p) {
$json_paragraphs[$p['index']] = [
'id' => 'default-paragraph-' . $p['index'],
'imageId' => $p['image_id'],
'title' => $p['title'],
'body' => $p['body'],
];
}

$json_paragraphs = json_encode($json_paragraphs);

$input_mythologies = array_map(function($m) {
return [
'label' => $m['name'],
'value' => $m['id']
];
}, $mythologies);

@endphp

<x-app-layout>

	<x-bladewind::modal name='confirm-modal-default' ok_button_action='onConfirmGlobal()' center_action_buttons="true" ok_button_label="Oui" cancel_button_label="Non">
		<h3 class='text-center text-2xl'>
			Voulez-vous vraiment effacer ce paragraphe ?
		</h3>
	</x-bladewind::modal>

	<x-slot name="header">
		{{ __('Editor') }}
	</x-slot>

	<x-scripts.confirm-modal />

	<x-navigation.hierarchy :hierarchy="$hierarchy" />

	<div x-data="paragraphListData" class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
			<form class='text-gray-200' enctype='multipart/form-data' method='POST' action='/god/{{ $god->id }}' id='main-form' @submit.prevent='submitGod'>
				@csrf
				@method('PATCH')

				@if (session('status'))
				<div class="bg-green-100 border border-green-400 text-green-700 mb-10 px-4 py-3 rounded relative" role="alert">
					{{ session('status') }}
				</div>
				@endif
				<div class='mb-6 flex justify-center text-gray-900'>
					<a target="_blank" class='btn-primary' href='{{ route('show.god', ['god' => $god->id]) }}'>
						Voir page
					</a>
				</div>

				<div class='bg-gray-800 p-9 shadow-md rounded-md'>
					<div class='w-full flex justify-center mb-6'>
						<textarea name='god-name' :rows='countRows(title)' class='resize-none bg-gray-700 text-center w-1/2' x-model='title'></textarea>
					</div>
					<div class='w-full flex justify-center items-center mb-3 gap-5 mb-6'>
						<div class='w-1/2'>
							<x-bladewind::select required='true' name="mythology_id" selected_value="{{$god->mythology->id}}" :data="$input_mythologies" />
						</div>
					</div>
					<div class='w-full flex justify-center'>
						<button type='button' x-text='isPublic ? "Publique" : "Privé"' @click='togglePublic' class='btn-outline-primary'></button>
						<input type="hidden" name="public" :value="isPublic ? 1 : 0">
					</div>
					<template x-for="(paragraph, i) in paragraphs">
						<div :key='paragraph.id' :id='paragraph.id'>
							<div class='my-5'>
								<button class='hover:text-danger-500 text-danger-400 transition' title="Delete Paragraph" tabindex='-1' type='button' @click='confirmModal("Voulez-vous vraiment effacer ce paragraphe ?", () => removeParagraph(paragraph.id), true)'>
									<x-icons.x size='5' />
								</button>
							</div>

							<div class='flex gap-x-10'>

								<div class='grow'>
									<div class='mb-3'>
										<label>Titre
											<textarea class='resize-none bg-gray-700 w-full' :name='`paragraphs[${i}][title]`' :rows='countRows(paragraph.title)' x-model='paragraph.title'></textarea>
										</label>
									</div>
									<div class='mb-3'>
										<label>Contenu
											<textarea class='resize-none bg-gray-700 w-full' :name='`paragraphs[${i}][body]`' :rows='countRows(paragraph.body)' x-model='paragraph.body'></textarea>
										</label>
									</div>
								</div>

								<div style='width: 200px'>
									<template x-if='paragraph.imageId !== null'>
										<div class='relative'>
											<a target="_blank" :href='`/show/img/${paragraph.imageId}`'>
												<img :src='`/show/img/${paragraph.imageId}?w=200`' />
											</a>
											<div class='absolute top-0 right-0'>
												<button class='m-1 p-1 rounded-full hover:bg-danger-700 bg-danger-700/75 text-danger-100 transition' title="Effacer Image" tabindex='-1' type='button' @click='paragraph.imageId = null;' style='left: -5px' >
													<x-icons.x size="4" />
												</button>
											</div>
										</div>
									</template>
									<template x-if='paragraph.imageId === null'>
										<div>
											<button class='btn-outline-primary' @click='selectImage($event, i)' type='button'>
												Choisir Image
											</button>
										</div>
									</template>
								</div>

							</div>


							<input :value='paragraph.imageId' :name='`paragraphs[${i}][image_id]`' type='hidden' />
						</div>
					</template>

					<div class='mt-5'>
						<button type='button' tabindex='0' @click="addParagraph($id)" class='btn-secondary flex gap-2' type="button">
							<x-icons.plus size='4' /> <span>Ajouter paragraphe</span>
						</button>
					</div>

				</div>

				<div class='mt-5'>
					<button type='submit' class='btn-primary'>Sauvegarder</button>
				</div>
			</form>
		</div>
	</div>

	<script>
		function emptyParagraph(id) {
			return {
				title: '',
				body: '',
				imageId: null,
				id: id,
			}
		};
		window.paragraphListData = {
			images: {},

			// returns n of rows for input to fit str
			countRows(str) {
				return (str.match(/\n/g) || []).length + 1;
			},

			// popup for image on paragraph
			async selectImage(event, i, url = '/imgs') {
				// button that launched this function
				const btn = event.srcElement;
				// btn dimensions
				const btnRect = btn.getBoundingClientRect();

				// div that will contain everything
				const imagesList = document.createElement('div');
				imagesList.classList.add(
					'bg-gray-700',
					'text-gray-200',
					'rounded',
					'w-72',
					'shadow',
					'p-2',
				);
				// Putting div next to button
				imagesList.style['position'] = 'absolute';
				imagesList.style['left'] = `${(btnRect.right + 10) | 0}px`;
				imagesList.style['top'] = `${(btnRect.top + window.scrollY) | 0}px`;

				// adding elem to body
				const body = document.querySelector('body');
				body.appendChild(imagesList);

				// wait after click to add remove event
				setTimeout(() => body.addEventListener('click', () => imagesList.remove()), 300);
				// do not remove element if clicked on it
				imagesList.addEventListener('click', (event) => event.stopPropagation());


				// contains search bar + btn
				const searchLine = document.createElement('div');
				searchLine.classList.add(
					'flex',
					'mb-1',
					'items-center');
				imagesList.appendChild(searchLine);

				// creating search bar
				const bar = document.createElement('input');
				searchLine.appendChild(bar);
				bar.classList.add(
					'h-8',
					'w-3/4',
					'bg-gray-700',
					'text-gray-100');

				// creating search button
				const searchButton = document.createElement('button');
				searchButton.type = 'button';
				searchButton.classList.add(
					'btn-outline-primary',
					'm-1',
					'p-1',
				);
				const imgClass = 'active:brightness-75 transition';
				searchButton.innerHTML = `
					<img
						width='25'
						class='${imgClass}'
						src='/images/lens.png' />`;
				searchLine.appendChild(searchButton);


				// to go to the manage images page directly
				const addButtonDiv = document.createElement('div');
				const addButton = document.createElement('a');
				addButton.innerHTML = 'Gérer';
				addButton.href = '/manage/images';
				addButton.target = '_blank';
				addButton.classList.add('btn-outline-primary', 'mb-2')
				addButtonDiv.appendChild(addButton);
				imagesList.appendChild(addButtonDiv);

				// if enter || btn clicked for search
				const searchFn = () => {
					imagesList.remove();
					const search = bar.value;
					this.selectImage(event, i, `/imgs?search=${search}`);
				}
				// adding fucntion hook
				searchButton.addEventListener('click', searchFn);
				bar.addEventListener('keypress', (e) => {
					if (e.key === 'Enter')
						searchFn();
				});

				// creating list of images
				const ul = document.createElement('ul');
				imagesList.appendChild(ul);

				// fetching images
				this.images = (await axios.get(url)).data;

				// filling list with images
				for (const x of this.images.data) {
					// li previews the image
					const li = document.createElement('li');
					ul.appendChild(li);
					li.innerHTML = `
						<img class='m-1 h-10'src="/show/img/${x.id}?h=40" />
						<div class='m-1 truncate' >${x.html_alt}</div>
						<div class='m-1 truncate' >${x.html_author}</div>`;
					// styling list
					li.classList.add(
						'flex',
						'items-center',
						'hover:bg-gray-400',
						'cursor-pointer',
						'transition',
						'rounded',
					);
					// setting img if clicked on it
					li.addEventListener('click', e => {
						this.paragraphs[i].imageId = x.id;
						imagesList.remove();
					});
				}

				// nav for pagination
				const nav = document.createElement('div');
				nav.classList.add('flex', 'items-center', 'justify-evenly');

				// buttons to go to other page
				const lButton = document.createElement('button');
				const rButton = document.createElement('button');

				// showing "1 / 4" pages for nav
				const pages = document.createElement('div');
				pages.innerHTML = `${this.images.current_page} / ${this.images.last_page}`;

				// appending elements
				nav.appendChild(lButton);
				nav.appendChild(pages);
				nav.appendChild(rButton);
				imagesList.appendChild(nav);

				// buttons are arrows
				lButton.innerHTML = '&lt;'
				rButton.innerHTML = '&gt;'
				// styling btns
				const btnStyles = [
					'p-2',
					'pe-3',
					'ps-3',
					'm-1',
					'hover:bg-gray-400',
					'cursor-pointer',
					'transition',
					'rounded-full',
				];
				lButton.classList.add(...btnStyles);
				rButton.classList.add(...btnStyles);
				// go to prev page
				if (this.images.prev_page_url) {
					lButton.addEventListener('click', () => {
						imagesList.remove();
						this.selectImage(event, i, this.images.prev_page_url);
					});
				}
				// go to next page
				if (this.images.next_page_url) {
					rButton.addEventListener('click', () => {
						imagesList.remove();
						this.selectImage(event, i, this.images.next_page_url);
					});
				}
			},

			title: '<?= str_replace("'", "\'", $god->name) ?>',
			isPublic: `{{ $god->public ? 'true' : 'false' }}` == 'true',
			mythology: '{{ $god->mythology->name }}',
			togglePublic() {
				this.isPublic = !this.isPublic;
			},
			paragraphs: <?= $json_paragraphs ?>,
			addParagraph(idGen) {
				this.paragraphs.push(emptyParagraph(idGen('paragraph')));
			},
			removeParagraph(id) {
				const index = this.paragraphs.findIndex((x) => id === x.id);
				this.paragraphs.splice(index, 1);
			},
			async submitGod() {
				document.getElementById('main-form').submit();
			},
		};
	</script>

</x-app-layout>
