<x-app-layout>
	<!-- Alpine Plugins -->
	<script src="https://cdn.jsdelivr.net/npm/@alpinejs/focus@3.13.2/dist/cdn.min.js"></script>

	<x-slot name="header">
		{{ __('Dashboard') }}
	</x-slot>

	<x-scripts.confirm-modal/>

	<div x-data='manageData' class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			<div class="h-screen shadow-sm sm:rounded-lg">
				<div class='text-white p-5'>

					<!-- Iterating for each tree rows -->
					<template x-for='(row, rindex) in mapHierarchy' :key='rindex'>
						<div class='grid grid-cols-12'>

							<!-- Dropdown -->
							<div class='border-r flex justify-center bg-gray-800 h-full col-span-2 py-7' x-data='{parentId: row.parent}'>
								<template x-if='row.parent !== null'>
									<div>
										<x-add-dropdown />
									</div>
								</template>
							</div>

							<!-- Tree row -->
							<div class='col-span-10 flex items-center py-7 justify-evenly'>

								<!-- Iterating for each tree column -->
								<template x-for='col in row.data' :key='col.uid'>
										<div class='flex items-start'>
											<!-- input to change name -->
											<input x-show='col.editing' x-trap='col.editing' :id='`edit-name-${col.id}`' @keydown.enter='setNewCatName(col)' :class='getElemClasses(col.type, col.id)' class="p-2 rounded" x-model='col.name'></button>
											<!-- button for god/category -->
											<button x-show='!col.editing' :class='getElemClasses(col.type, col.id)' @click='elemOnClick(col, row)' class="p-2 rounded" x-text='col.name'></button>
											<!-- delete button -->
											<template x-if='!(col.type === "category" && col.id === 1)'>
												<button class='btn-danger p-1 ms-1 text-xs' @click='deletePopup(col.type, col.id, row.parent)'>
													<x-icons.x />
												</button>
											</template>
										</div>
								</template>

							</div>

						</div>
					</template>


					<!-- Last dropdown if selected category is empty -->
					<template x-if='shouldAddDropDown'>
						<div class='grid grid-cols-12'>
							<div class='col-span-2' x-data='{parentId: focusedCategoryId}'>
								<x-add-dropdown parentId='focusedCategoryId' />
							</div>
						</div>
					</template>


				</div>
			</div>
		</div>
	</div>
</x-app-layout>


<script>
	window.manageData = {
		// Maps of format {id: {object}, ...}
		categories: new Map(),
		gods: new Map(),

		isInSelectChain: function (elemType, id) {
			if (elemType === 'god') {
				return id === this.focusedGodId;
			}
			let ptr = this.focusedCategoryId;
			while (ptr != null) {
				if (id === ptr) return true;
				ptr = this.categories.get(ptr).parent_id;
			}
			return false;
		},


		// style tree elements according to focus and id
		getElemClasses: function(elemType, id) {
			const isSelect = this.isInSelectChain(elemType, id);

			switch (elemType) {
				case 'category':
					if (isSelect)
						return "bg-yellow-600";
					return "bg-yellow-800";
					break;
				case 'god':
					if (isSelect)
						return "bg-red-700";
					return "bg-red-900";
					break;
			}
		},

		// act for element click according to type and id
		elemOnClick: function(col, row) {
			switch (col.type) {
				case 'category':
					// double click
					if (this.focusedCategoryId === col.id) {
						col.editing = true; // edit name of category
						this.oldName = col.name; // keep in memory name before edit
						// reset name and stop editing when focus is lost
						const input = document.getElementById(`edit-name-${col.id}`);
						input.addEventListener("blur", () => {
							col.editing = false;
							col.name = this.oldName;
						});
						return;
					}
					this.focusedCategoryId = col.id;
					this.focusedGodId = 0;
					break;
				case 'god':
					// double click
					if (this.focusedGodId === col.id) {
						// go to god editor
						window.location = `/manage/god/${col.id}`;
						return
					}
					this.focusedCategoryId = row.parent;
					this.focusedGodId = col.id;
					break;
			}
		},

		// keep in memory the name of edited category
		oldName: '',

		// delete component
		deleteElement: async function(elemType, id, parentId) {
			const uri = `/${elemType}/${id}`;
			const returnCode = (await axios.delete(uri)).status;

			// Is responde ok
			if (Math.floor(returnCode / 100) !== 2)
				return;

			// no need to refresh category if god is deleted
			if (elemType === 'category') {
				this.refreshCategories();
			}
			// children god are also deleted
			this.refreshGods();

			// check if foxued elems have been deleted
			const isGodDeleted = (
				this.focusedGodId !== 0 && !this.gods.has(this.focusedGodId)
			);
			const isCategoryDeleted = (
				!this.categories.has(this.focusedCategoryId)
			);

			// update foxus if focused elems have been deleted
			if (isGodDeleted)
				this.focusedGodId = 0;
			if (isCategoryDeleted)
				this.focusedCategoryId = parentId;
		},

		// fill tree at launch
		init: function() {
			this.refreshCategories();
			this.refreshGods();
		},

		// add last dropdown if selected category is empty
		get shouldAddDropDown() {
			const mh = this.mapHierarchy;
			const lastRow = mh[mh.length - 1].data;
			return lastRow.some(x => (
				x.type === 'category' && x.id === this.focusedCategoryId
			));
		},

		// fetch all gods and update tree
		refreshGods: async function() {
			// empty god map
			this.gods = new Map();

			console.log('refreshing gods');

			// fetch data and fill Map
			const gods = (await axios.get('/gods')).data;
			for (const god of gods) {
				this.gods.set(god.id, {
					parent_id: god.category_id,
					name: god.name,
				})
			}
		},

		// change category name
		setNewCatName: async function(col) {
			// enter is pressed - name is now changed
			this.oldName = col.name;
			await axios.patch(`/category/${col.id}`, {
				name: col.name,
			});
			col.editing = false;
		},

		// fetch all categories and update tree
		refreshCategories: async function() {
			// empty categories map
			this.categories = new Map();

			// fetch data and fill Map
			const categories = (await axios.get('/categories')).data;
			for (const category of categories) {
				this.categories.set(category.id, {
					parent_id: category.parent_id,
					name: category.name,
					editing: false,
				})
			}
		},

		// category and god id that are in focus (mouse click)
		focusedCategoryId: 1,
		focusedGodId: 0,

		// array representing tree rows/columns as an array
		get mapHierarchy() {
			// current parent of row to add to array
			let parent = this.focusedCategoryId;
			// return value
			const tree = [];

			// for each category -> add its children. Repeat for its parent
			while (parent !== null) {
				// current row
				const to_push = [];

				// add all child categories
				for (const [key, value] of this.categories.entries()) {
					if (value.parent_id !== parent)
						continue;
					to_push.push({
						id: key,
						...value,
						'type': 'category',
						uid: `category-${key}`, // for template :key
					});
				}

				// Add all child gods
				for (const [key, value] of this.gods.entries()) {
					if (value.parent_id !== parent)
						continue;
					to_push.push({
						id: key,
						...value,
						'type': 'god',
						uid: `god-${key}`, // for template :key
					});
				}

				// to prevent array empty bug
				if (to_push.length !== 0) {
					// adding row to tree
					tree.push({
						'parent': parent, // parent of row
						data: to_push // row elements
					});
				}

				// to prevent array empty bug
				parent = this.categories.get(parent)?.parent_id;
				// chagne parent for next row
				parent = (parent === undefined ? null : parent);
			}

			// add root category "Mytholoteque"
			const rootId = 1;
			const rootCategory = this.categories.get(rootId);
			tree.push({
				'parent': null,
				'data': [{
					id: rootId,
					...rootCategory,
					'type': 'category',
					uid: `category-${rootId}`,
				}]
			});

			return tree.reverse();
		},

		// asks to comfirm for delete
		deletePopup(colType, colId, rowParent) {
			// gray cover
			const blackScreen = document.createElement('div');
			blackScreen.classList.add(
				'w-screen',
				'h-screen',
				'absolute',
				'bottom-0',
				'left-0',
				'bg-black/50',
				'flex',
				'justify-center',
				'items-center',
			);
			blackScreen.id = 'confirm-popup';

			// "yes" is pressed
			const action = async () => {
				await this.deleteElement(colType, colId, rowParent);
				// removing cover
				blackScreen.remove();
			}
			// "no", or outside is pressed
			const cancel = (event, elem) => {
				// if children is clicked, do nothing
				if (elem !== event.target) return;
				// removing cover
				blackScreen.remove();
			}

			// Title of panel
			const ask = document.createElement('div');
			ask.innerHTML = `Do you really want to delete this ${colType}?`;

			// panel over cover
			const panel = document.createElement('div');
			panel.classList.add(
				'text-2xl',
				'text-gray-100',
				'bg-gray-800',
				'h-1/4',
				'flex',
				'flex-col',
				'items-center',
				'justify-center',
				'p-12',
				'rounded-lg',
			);

			// "yes" button
			const buttons = document.createElement('div');
			buttons.classList.add(
				'flex',
				'justify-evenly',
				'w-full',
				'mt-10',
			);
			const yes = document.createElement('button');
			yes.type = 'button';
			yes.innerHTML = 'Yes';
			// delete element if "yes" is pressed
			yes.addEventListener('click', action);

			// "no" button
			const no = document.createElement('button');
			no.type = 'button';
			no.innerHTML = 'No';

			// styling buttons
			const btnStyle = ['p-3', 'rounded'];
			no.classList.add('btn-outline-primary', 'text-xl');
			yes.classList.add('btn-outline-danger', 'text-xl');

			// elems that cancels deletion
			blackScreen.addEventListener(
				'click', () => cancel(event, blackScreen)
			);
			no.addEventListener('click', () => cancel(event, no));

			// appending elements
			buttons.appendChild(no);
			buttons.appendChild(yes);
			panel.appendChild(ask);
			panel.appendChild(buttons);
			blackScreen.appendChild(panel);
			document.body.appendChild(blackScreen);
		}

	};
</script>
