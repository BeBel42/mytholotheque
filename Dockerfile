# Image to deploy laravel in production with nginx, mariadb and tailwind CSS
# Based on Laravel Sail's image for php 8.3

# Additional notes:
# the APP_DEBUG enb variable must be set to false
# info taken from https://laravel.com/docs/10.x/deployment#introduction

# same base image as laravel sail
FROM ubuntu:22.04

# for laravel deps installation
ARG NODE_VERSION=18

# so that debian does not ask for region
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install all laravel dependencies
RUN apt-get update \
    && mkdir -p /etc/apt/keyrings \
    && apt-get install -y gnupg gosu curl ca-certificates zip unzip git supervisor sqlite3 libcap2-bin libpng-dev dnsutils librsvg2-bin fswatch \
    && curl -sS 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x14aa40ec0831756756d7f66c4f4ea0aae5267a6c' | gpg --dearmor | tee /etc/apt/keyrings/ppa_ondrej_php.gpg > /dev/null \
    && echo "deb [signed-by=/etc/apt/keyrings/ppa_ondrej_php.gpg] https://ppa.launchpadcontent.net/ondrej/php/ubuntu jammy main" > /etc/apt/sources.list.d/ppa_ondrej_php.list \
    && apt-get update \
    && apt-get install -y php8.3-cli php8.3-dev \
    php8.3-sqlite3 php8.3-gd \
    php8.3-curl \
    php8.3-imap php8.3-mysql php8.3-mbstring \
    php8.3-xml php8.3-zip php8.3-bcmath php8.3-soap \
    php8.3-intl php8.3-readline \
    php8.3-ldap \
    && curl -sLS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_VERSION.x nodistro main" > /etc/apt/sources.list.d/nodesource.list \
    && apt-get update \
    && apt-get install -y nodejs \
    && npm install -g npm \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /etc/apt/keyrings/yarn.gpg >/dev/null \
    && echo "deb [signed-by=/etc/apt/keyrings/yarn.gpg] https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y yarn \
    && apt-get install -y mysql-client \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copied from laravel sail
RUN setcap "cap_net_bind_service=+ep" /usr/bin/php8.3

# Install required packages for PHP-FPM and extensions
RUN apt-get update && \
    apt-get install -y php8.3-fpm && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install nginx
RUN apt-get update && apt-get install -y nginx

# moving files to corresponding directories
WORKDIR /var/www/
COPY . .
RUN mv ./nginx/deployment.conf /etc/nginx/nginx.conf \
    && mv ./php-fpm.conf /etc/php-fpm.conf

# install php libraries
RUN composer update \
    && composer install --optimize-autoloader --no-dev

# install js libraties (and compile CSS)
RUN npm i && npm run build

# production optimisations
RUN php artisan config:cache \
    && php artisan event:cache \
    && php artisan route:cache \
    && php artisan view:cache

# Set up permissions
RUN chown -R www-data:www-data /var/www

# migrate db, then launch php and nginx
CMD php artisan migrate \
    && php-fpm8.3 -y /etc/php-fpm.conf -D \
    && nginx -g 'daemon off;'

# port in nginx config file
EXPOSE 80
