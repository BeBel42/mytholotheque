<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Reverts add_category_mythology
 */

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::table('categories', function (Blueprint $table) {
			$table->dropForeign(['mythology_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		if (!Schema::hasColumn('categories', 'mythology_id')) {
			Schema::table('categories', function (Blueprint $table) {
				$table->foreignId('mythology_id')->default(0)->constrained();
			});
		}
	}
};
