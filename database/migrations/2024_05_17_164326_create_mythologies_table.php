<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Mythology;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('mythologies', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
		});

		// Insert default mythologies
		Mythology::firstOrCreate(['name' => 'grecque']);
		Mythology::firstOrCreate(['name' => 'romaine']);
		Mythology::firstOrCreate(['name' => 'égyptienne']);
		Mythology::firstOrCreate(['name' => 'nordique']);
		Mythology::firstOrCreate(['name' => 'japonaise']);
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::dropIfExists('mythologies');
	}
};
