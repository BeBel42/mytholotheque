<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paragraphs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedInteger('index');
            $table->string('title');
            $table->longText('body');
            $table->foreignId('god_id')
				->constrained()
				->onDelete('cascade');
            $table->foreignId('image_id')
				->nullable()
				->constrained();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
		Schema::table('paragraphs', function (Blueprint $table) {
			$table->dropForeign(['iamge_id']);
			$table->dropForeign(['god_id']);
		});
        Schema::dropIfExists('paragraphs');
    }
};
