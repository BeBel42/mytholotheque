<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('images', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->binary('data');
			$table->text('alt');
			$table->text('author');
		});
		// increase max size to 16M
		DB::statement('ALTER TABLE images MODIFY data MEDIUMBLOB');
	}

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('images');
    }
};
