<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('gods', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->string('name');
			$table->foreignId('category_id')
				->constrained()
				->onDelete('cascade');
			$table->boolean('public');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::table('gods', function (Blueprint $table) {
			$table->dropForeign(['category_id']);
		});
		Schema::dropIfExists('gods');
	}
};
