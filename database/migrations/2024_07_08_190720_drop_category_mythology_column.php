<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/*
 * continuation of remove_category_mythology_column
 */

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::table('categories', function (Blueprint $table) {
			$table->dropColumn(['mythology_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		// in remove_category_mythology_column
	}
};
