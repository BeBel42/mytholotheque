<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('gods', 'mythology_id')) {
			Schema::table('gods', function (Blueprint $table) {
				$table->foreignId('mythology_id')->default(0)->constrained();
			});
		}
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
		Schema::table('gods', function (Blueprint $table) {
			$table->dropForeign(['mythology_id']);
		});
    }
};
