<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('threads', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->text('title');
			$table->foreignid('god_id')
				->constrained()
				->onDelete('cascade');
			$table->foreignid('user_id')
				->constrained()
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::table('threads', function (Blueprint $table) {
			$table->dropForeign(['user_id']);
			$table->dropForeign(['god_id']);
		});
		Schema::dropIfExists('threads');
	}
};
