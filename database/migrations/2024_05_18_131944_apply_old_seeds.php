<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Category;
use \App\Models\Role;
use \App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Mythology;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{

		// create root category
		$greek_mythology_id = Mythology::where('name', 'grecque')->value('id');
		Category::upsert([
			'name' => 'Mytholotheque',
			'mythology_id' => $greek_mythology_id,
			'id' => 1,
		], []);

		// creating user roles
		foreach (['administrator', 'moderator', 'regular'] as $role) {
			Role::firstOrCreate(['name' => $role]);
		}

		// createing admin user roles
		$admin_role_id = Role::where('name', 'administrator')->value('id');
		$admin_users = [
			[
				'name' => 'Martin',
				'email' => 'contact@mlefevre.xyz',
				'password' => Hash::make('mytholotheque'),
				'role_id' => $admin_role_id,
				'profile_picture' => null,
			], [
				'name' => 'Valentine',
				'email' => 'vali.baeyens@gmail.com',
				'password' => Hash::make('mytholotheque'),
				'role_id' => $admin_role_id,
				'profile_picture' => null,
			]
		];
		// create users only if don't exist yet
		foreach ($admin_users as $user) {
			if (!count(User::where('email', $user['email'])->get())) {
				$user_model = User::create($user);
				$user_model->markEmailAsVerified();
			}
		}
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		// Delete the Category record if it exists
		Category::where('name', 'Mytholotheque')->delete();

		// Delete User records
		User::whereIn('email', [
			'contact@mlefevre.xyz', 'vali.baeyens@gmail.com'
		])->delete();

		// Delete Role records
		foreach (['administrator', 'moderator', 'regular'] as $role) {
			Role::where('name', $role)->delete();
		}
	}
};
