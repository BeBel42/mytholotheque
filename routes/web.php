<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GodController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\CommentController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider and all of them will
  | be assigned to the "web" middleware group. Make something great!
  |
*/

// root
Route::get('/', function () {
    return view('welcome');
});

// God page for anyone
Route::get('/show/god/{god}', [GodController::class, 'show'])
	->name('show.god');
// Image for anyone
Route::get('/show/img/{image}', [ImageController::class, 'show'])
	->name('show.img');
// FAQ for anyone
Route::get('/faq', function() {
	return view('faq');
})->name('faq');
// Category page for anyone
Route::get('/show/category/{category}', [CategoryController::class, 'show'])
	->name('show.category');

Route::get('/show/threads/{god}', [ThreadController::class, 'index'])
	->name('show.threads');
Route::get('/show/thread/{thread}', [ThreadController::class, 'show'])
	->name('show.thread');

// logged in, but not email-verified
Route::middleware('auth')->group(function () {
	// profile routes
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

// logged in, and email-verified
Route::middleware(['auth', 'verified'])->group(function () {
	// Admin manager
    Route::get('/manage/god/{god}', [GodController::class, 'manage'])
		->name('manage.god');
	Route::get('/manage/images', [ImageController::class, 'manage'])
		->name('manage.images');
    Route::get('/manage/categories', function () {
        return view('manage.categories');
    })->name('manage.categories');

	// dashboard for all users
	Route::get('/dashboard', function () {
		return view('dashboard');
	})->name('dashboard');
});

// api - some return JSON, some redirect (for form submit)
Route::middleware(['auth', 'verified'])->group(function () {
    // categories
    Route::get('/categories', [CategoryController::class, 'all']);
    Route::post('/category', [CategoryController::class, 'post']);
    Route::delete('/category/{category}', [CategoryController::class, 'delete']);
    Route::patch('/category/{category}', [CategoryController::class, 'patch']);

    // gods
    Route::get('/gods', [GodController::class, 'all']);
    Route::post('/god', [GodController::class, 'post']);
    Route::delete('/god/{god}', [GodController::class, 'delete']);
    Route::patch('/god/{god}', [GodController::class, 'patch']);

	// images
    Route::get('/imgs', [ImageController::class, 'json_paginate']);
    Route::post('/img', [ImageController::class, 'post']);
    Route::delete('/img/{image}', [ImageController::class, 'delete']);
    Route::patch('/img/{image}', [ImageController::class, 'patch']);
	// profile pictures
    Route::get('/pfp/{user}', [ProfileController::class, 'get_profile_picture']);
    Route::patch('/pfp', [ProfileController::class, 'set_profile_picture']);

	// Threads
    Route::post('/thread', [ThreadController::class, 'post']);
    Route::delete('/thread/{thread}', [ThreadController::class, 'delete']);
    Route::patch('/thread/{thread}', [ThreadController::class, 'patch']);
    Route::post('/comment', [CommentController::class, 'post']);
    Route::delete('/comment/{comment}', [CommentController::class, 'delete']);
    Route::patch('/comment/{comment}', [CommentController::class, 'patch']);
});

require __DIR__.'/auth.php';
